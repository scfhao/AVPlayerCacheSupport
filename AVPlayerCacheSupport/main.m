//
//  main.m
//  AVPlayerCacheSupport
//
//  Created by xueyi on 2017/8/24.
//  Copyright © 2017年 https://scfhao.coding.me/blog/. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
