//
//  ViewController.m
//  AVPlayerCacheSupport
//
//  Created by xueyi on 2017/8/24.
//  Copyright © 2017年 https://scfhao.coding.me/blog/. All rights reserved.
//

#import "ViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "AVPlayerItem+MCCacheSupport.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UISlider *slider;
@property (strong, nonatomic) AVPlayer *player;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (NSString *)cachePath {
    NSString *documents = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)lastObject];
    return [documents stringByAppendingPathComponent:@"audioCache"];
}

- (IBAction)playAction:(id)sender {
    NSURL *url = [NSURL URLWithString:@"http://www.gkbbapp.com/Support/Download.aspx?DownloadVideo=2015/12/2830_7386821873.mp4&USERID=123&version=827"];
    NSString *cachePath = [self cachePath];
    AVPlayerItem *item = [AVPlayerItem mc_playerItemWithRemoteURL:url options:nil cacheFilePath:cachePath error:nil];
    self.player = [[AVPlayer alloc]initWithPlayerItem:item];
    __weak ViewController *ws = self;
    [self.player addPeriodicTimeObserverForInterval:CMTimeMake(1, 1) queue:dispatch_get_main_queue() usingBlock:^(CMTime time) {
        ws.slider.value = CMTimeGetSeconds(time) / CMTimeGetSeconds(ws.player.currentItem.duration);
    }];
    [self.player playImmediatelyAtRate:1];
}

- (IBAction)clearAction:(id)sender {
    [AVPlayerItem mc_removeCacheWithCacheFilePath:[self cachePath]];
}

- (IBAction)stopAction:(id)sender {
    [self.player replaceCurrentItemWithPlayerItem:nil];
    self.player = nil;
}

- (IBAction)sliderAction:(UISlider *)sender {
    [self.player seekToTime:CMTimeMake(CMTimeGetSeconds(self.player.currentItem.duration) * sender.value, 1)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
