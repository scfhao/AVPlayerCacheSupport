//
//  MCAVPlayerItemCacheTask.h
//  AVPlayerCacheSupport
//
//  Created by Chengyin on 16/3/21.
//  Copyright © 2016年 Chengyin. All rights reserved.
//

#import <Foundation/Foundation.h>
@class MCAVPlayerItemCacheFile;
@class AVAssetResourceLoadingRequest;
@class MCAVPlayerItemCacheTask;

typedef void (^MCAVPlayerItemCacheTaskFinishedBlock)(MCAVPlayerItemCacheTask *task,NSError *error);

@interface MCAVPlayerItemCacheTask : NSOperation
{
@protected
    AVAssetResourceLoadingRequest *_loadingRequest;
    NSRange _range;
    MCAVPlayerItemCacheFile *_cacheFile;
}

@property (nonatomic,copy) MCAVPlayerItemCacheTaskFinishedBlock finishBlock;

- (instancetype)initWithCacheFile:(MCAVPlayerItemCacheFile *)cacheFile loadingRequest:(AVAssetResourceLoadingRequest *)loadingRequest range:(NSRange)range;

@end
