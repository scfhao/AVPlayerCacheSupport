//
//  AppDelegate.h
//  AVPlayerCacheSupport
//
//  Created by xueyi on 2017/8/24.
//  Copyright © 2017年 https://scfhao.coding.me/blog/. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

